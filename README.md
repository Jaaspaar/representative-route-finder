# Representative Route Finder

Program calculates the representative route defined as a route that has the smallest sum of Hausdorff distances to other routes.

Run  
```mvn install```  
to install dependencies.

Run   
```mvn compile exec:java```  
to execute application.

