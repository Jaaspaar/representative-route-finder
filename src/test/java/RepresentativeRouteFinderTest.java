import logic.DistanceCalculator;
import logic.RepresentativeRouteFinder;
import models.Route;
import models.RouteDistance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static net.obvj.junit.utils.matchers.AdvancedMatchers.throwsException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RepresentativeRouteFinderTest {
    RepresentativeRouteFinder representativeRouteFinder;
    DistanceCalculator distanceCalculator;
    Map<Route, Map<Route, RouteDistance>> routesDistance;
    List<Route> routes;

    @BeforeEach
    void setUp() {
        routes = new LinkedList<>();
        routesDistance = generateRoutesDistance();
        distanceCalculator = Mockito.mock(DistanceCalculator.class);
        Mockito.when(distanceCalculator.calculateDistanceForRoutes(Mockito.eq(routes))).thenReturn(routesDistance);
        representativeRouteFinder = new RepresentativeRouteFinder(distanceCalculator);
    }

    @Test
    void shouldReturnIdOfRouteWithSmallerTotalDistanceWhenGetIdOfRouteWithTheSmallestTotalDistanceIsCalled() {
        String routeId = representativeRouteFinder.getIdOfRouteWithTheShortestTotalDistance(routes);

        assertThat(routeId, is(equalTo("id_1")));
    }

    @Test
    public void shouldThrowWhenNullPassedAsRoutes() {
        assertThat(() -> representativeRouteFinder.getIdOfRouteWithTheShortestTotalDistance(null),
                throwsException(IllegalArgumentException.class));
    }

    private Map<Route, Map<Route, RouteDistance>> generateRoutesDistance() {
        Route firstRoute = Mockito.mock(Route.class);
        Mockito.when(firstRoute.getId()).thenReturn("id_1");
        Route secondRoute = Mockito.mock(Route.class);
        Mockito.when(secondRoute.getId()).thenReturn("id_2");
        Route thirdRoute = Mockito.mock(Route.class);
        Mockito.when(thirdRoute.getId()).thenReturn("id_3");

        RouteDistance firstToSecondRouteDistance = Mockito.mock(RouteDistance.class);
        RouteDistance firstToThirdRouteDistance = Mockito.mock(RouteDistance.class);
        RouteDistance secondToThirdRouteDistance = Mockito.mock(RouteDistance.class);
        Mockito.when(firstToSecondRouteDistance.getDistance()).thenReturn(1.0);
        Mockito.when(firstToThirdRouteDistance.getDistance()).thenReturn(2.0);
        Mockito.when(secondToThirdRouteDistance.getDistance()).thenReturn(3.0);

        return Map.of(firstRoute, Map.of(secondRoute, firstToSecondRouteDistance, thirdRoute, firstToThirdRouteDistance),
                secondRoute, Map.of(firstRoute, firstToSecondRouteDistance, thirdRoute, secondToThirdRouteDistance),
                thirdRoute, Map.of(firstRoute, firstToThirdRouteDistance, secondRoute, secondToThirdRouteDistance));
    }
}
