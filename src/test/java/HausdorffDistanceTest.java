import logic.HausdorffDistance;
import org.junit.jupiter.api.Test;

import java.awt.geom.Point2D;
import java.util.List;

import static net.obvj.junit.utils.matchers.AdvancedMatchers.throwsException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class HausdorffDistanceTest {

    @Test
    public void shouldReturnMaxSquaredEuclideanDistanceWhenCalculateMaxDistanceIsCalled() {
        Point2D p1 = new Point2D.Double(1,1);
        Point2D p2 = new Point2D.Double(2, 2);
        Point2D p3 = new Point2D.Double(3, 3);

        List<Point2D> points = List.of(p1, p2);
        List<Point2D> otherPoints = List.of(p3);

        final double maxDistance = HausdorffDistance.calculateLongestDistance(points, otherPoints);
        assertThat(maxDistance, is(equalTo(8.0)));
    }

    @Test
    public void shouldThrowWhenNullPassedAsFirstList() {
        assertThat(() -> HausdorffDistance.calculateLongestDistance(null, List.of()),
                throwsException(IllegalArgumentException.class));
    }

    @Test
    public void shouldThrowWhenNullPassedAsSecondList() {
        assertThat(() -> HausdorffDistance.calculateLongestDistance(List.of(), null),
                throwsException(IllegalArgumentException.class));
    }
}
