import logic.DistanceCalculator;
import models.Route;
import models.RouteDistance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;

import static net.obvj.junit.utils.matchers.AdvancedMatchers.throwsException;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DistanceCalculatorTest {
    Route firstRoute, secondRoute;
    DistanceCalculator distanceCalculator;
    List<Route> routes;

    @BeforeEach
    public void setUp() {
        distanceCalculator = new DistanceCalculator();
        routes = generateRoutes();
    }

    @Test
    public void shouldCalculateDistanceForAllRoutesWhenCalculateDistanceForRoutesIsCalled() {
        var routesDistance = distanceCalculator.calculateDistanceForRoutes(routes);

        assertThat(routesDistance.size(), is(equalTo(2)));
    }

    @Test
    public void shouldCalculateDistanceForFirstRouteToSecondRouteWhenCalculateDistanceForRoutesIsCalled() {
        var routesDistance = distanceCalculator.calculateDistanceForRoutes(routes);

        RouteDistance routeDistance = routesDistance.get(firstRoute).get(secondRoute);
        assertThat(routeDistance.getDistance(), is(equalTo(8.0)));
    }

    @Test
    public void shouldUseTheSameDistanceObjectForFirstAndSecondRouteWhenCalculateDistanceForRoutesIsCalled() {
        var routesDistance = distanceCalculator.calculateDistanceForRoutes(routes);

        RouteDistance firstRouteDistance = routesDistance.get(firstRoute).get(secondRoute);
        RouteDistance secondRouteDistance = routesDistance.get(secondRoute).get(firstRoute);
        assertThat(firstRouteDistance, is(sameInstance(secondRouteDistance)));
    }

    @Test
    public void shouldThrowWhenNullPassedAsRoutes() {
        assertThat(() -> distanceCalculator.calculateDistanceForRoutes(null),
                throwsException(IllegalArgumentException.class));
    }

    private List<Route> generateRoutes() {
        List<Point2D> points = Arrays.asList(new Point2D.Double(1, 1), new Point2D.Double(2, 2));
        List<Point2D> otherPoints = Arrays.asList(new Point2D.Double(3, 3));

        firstRoute = new Route("id_1", points);
        secondRoute = new Route("id_2", otherPoints);

        return Arrays.asList(firstRoute, secondRoute);
    }
}
