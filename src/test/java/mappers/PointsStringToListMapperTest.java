package mappers;

import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.geom.Point2D;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class PointsStringToListMapperTest {
    PointsStringToListMapper pointsStringToListMapper;

    @BeforeEach
    void setUp() {
        pointsStringToListMapper = new PointsStringToListMapper();
    }

    @Test
    void shouldReturnArrayOfPointsWhenConvertIsCalled() {
        final String pointsAsString = "[[8.489074, 53.615707, 1509423228430, 14.0], " +
                "[8.476499, 53.621193, 1509423365984, 14.8], [8.463407, 53.626442, 1509423498599, 15.8]]";
        final List<Point2D> expectedPoints = List.of(new Point2D.Double(8.489074, 53.615707),
                new Point2D.Double(8.476499, 53.621193), new Point2D.Double(8.463407, 53.626442));

        try {
            List<Point2D> points = pointsStringToListMapper.convert(pointsAsString);
            assertThat(points, is(equalTo(expectedPoints)));
        } catch (CsvDataTypeMismatchException e) {
            e.printStackTrace();
        } catch (CsvConstraintViolationException e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldReturnEmptyListWhenConvertIsCalledWithNull() {
        try {
            List<Point2D> points = pointsStringToListMapper.convert(null);
            assertThat(points, hasSize(0));
        } catch (CsvDataTypeMismatchException e) {
            e.printStackTrace();
        } catch (CsvConstraintViolationException e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldReturnEmptyListWhenConvertIsCalledWithEmptyString() {
        try {
            List<Point2D> points = pointsStringToListMapper.convert("");
            assertThat(points, hasSize(0));
        } catch (CsvDataTypeMismatchException e) {
            e.printStackTrace();
        } catch (CsvConstraintViolationException e) {
            e.printStackTrace();
        }
    }
}
