package models;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RouteTest {

    @Test
    void shouldReturnTrueWhenTheSameObjectsAreCompared() {
        Route firstRoute = new Route("id_1", List.of());
        boolean areEqual = firstRoute.equals(firstRoute);

        assertThat(areEqual, is(equalTo(true)));
    }

    @Test
    void shouldReturnFalseWhenTheObjectIsComparedWithNull() {
        Route firstRoute = new Route("id_1", List.of());
        boolean areEqual = firstRoute.equals(null);

        assertThat(areEqual, is(equalTo(false)));
    }

    @Test
    void shouldReturnTrueWhenTwoObjectsWithTheSameIdAreCompared() {
        Route firstRoute = new Route("id_1", List.of());
        Route anotherFirstRoute = new Route("id_1", List.of());
        boolean areEqual = firstRoute.equals(anotherFirstRoute);

        assertThat(areEqual, is(equalTo(true)));
    }

    @Test
    void shouldReturnFalseWhenTwoObjectsWithDifferentIdsAreCompared() {
        Route firstRoute = new Route("id_1", List.of());
        Route anotherFirstRoute = new Route("id_2", List.of());
        boolean areEqual = firstRoute.equals(anotherFirstRoute);

        assertThat(areEqual, is(equalTo(false)));
    }
}
