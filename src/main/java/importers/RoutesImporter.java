package importers;

import com.opencsv.bean.CsvToBeanBuilder;
import models.Route;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Parses CVS file and map routes to {@link Route} entity
 */
public class RoutesImporter {
    public static List<Route> importRoutesFromCsv(String fileName) {
        try {
            CsvToBeanBuilder<Route> beanBuilder = new CsvToBeanBuilder<>(new InputStreamReader(new FileInputStream(fileName)));

            return beanBuilder
                    .withType(Route.class)
                    .build()
                    .parse();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return null;
    };
}
