package logic;

import models.RouteDistance;
import models.Route;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DistanceCalculator {

    /**
     * Calculates map of routes with corresponding buckets that contain information about distance to any other route
     * @param routes list of routes
     * @return map of routes with corresponding buckets
     */
    public Map<Route, Map<Route, RouteDistance>> calculateDistanceForRoutes(List<Route> routes) {
        if (routes == null)
            throw new IllegalArgumentException("No routes provided");

        var routesDistanceHandler = new RoutesDistanceHandler();
        for (var route: routes) {
            for (var otherRoute: routes) {
                RouteDistance routeDistance = routesDistanceHandler.findDistanceBetweenRoutes(route, otherRoute);
                if (routeDistance != null)
                    break;

                routeDistance = calculateDistanceBetweenRoutes(route, otherRoute);
                var routeBucket = routesDistanceHandler.getRouteBucket(route);
                if (routeBucket == null) {
                    routeBucket = new HashMap<>();
                    routesDistanceHandler.put(route, routeBucket);
                }
                routeBucket.put(otherRoute, routeDistance);

                var otherRouteBucket = routesDistanceHandler.getRouteBucket(otherRoute);
                if (otherRouteBucket == null) {
                    otherRouteBucket = new HashMap<>();
                    routesDistanceHandler.put(otherRoute, otherRouteBucket);
                }
                otherRouteBucket.put(route, routeDistance);
            }
        }
        return routesDistanceHandler.getRoutesDistance();
    }

    private RouteDistance calculateDistanceBetweenRoutes(Route route, Route otherRoute) {
        double distance = HausdorffDistance.calculateLongestDistance(route.getPoints(), otherRoute.getPoints());
        return new RouteDistance(route.getId(), otherRoute.getId(), distance);
    }

    private class RoutesDistanceHandler {
        private final Map<Route, Map<Route, RouteDistance>> routesDistance;

        RoutesDistanceHandler() {
            this.routesDistance = new HashMap<>();
        }

        RouteDistance findDistanceBetweenRoutes(Route route, Route otherRoute) {
            var routeBucket = routesDistance.get(route);
            return routeBucket != null ? routeBucket.get(otherRoute) : null;
        }

        Map<Route, RouteDistance> getRouteBucket(Route route) {
            return routesDistance.get(route);
        }

        Map<Route, RouteDistance> put(Route route, Map<Route, RouteDistance> routeBucket) {
            return routesDistance.put(route, routeBucket);
        }

        Map<Route, Map<Route, RouteDistance>> getRoutesDistance() {
            return Map.copyOf(routesDistance);
        }
    }
}
