package logic;

import models.RouteDistance;
import models.Route;

import java.util.List;
import java.util.Map;

public class RepresentativeRouteFinder {
    final private DistanceCalculator distanceCalculator;

    public RepresentativeRouteFinder(DistanceCalculator distanceCalculator) {
        this.distanceCalculator = distanceCalculator;
    }

    /**
     * Returns ID of a route that has smallest sum of Hausdorff distances to other routes
     * @param routes
     * @return route ID
     */
    public String getIdOfRouteWithTheShortestTotalDistance(List<Route> routes) {
        if (routes == null)
            throw new IllegalArgumentException("No routes provided");

        Map<Route, Map<Route, RouteDistance>> routesDistance = distanceCalculator.calculateDistanceForRoutes(routes);
        double minTotalDistance = Double.MAX_VALUE;
        Route minTotalDistanceRoute = null;

        for (var routeDistanceEntry: routesDistance.entrySet()) {
            double totalDistance = 0;
            for (var routeDistance: routeDistanceEntry.getValue().values())
                totalDistance += routeDistance.getDistance();
            if (totalDistance < minTotalDistance) {
                minTotalDistance = totalDistance;
                minTotalDistanceRoute = routeDistanceEntry.getKey();
            }
        }

        return minTotalDistanceRoute != null ? minTotalDistanceRoute.getId() : "";
    }
}
