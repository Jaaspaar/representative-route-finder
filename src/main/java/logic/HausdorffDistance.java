package logic;

import java.awt.geom.Point2D;
import java.util.List;

public class HausdorffDistance {
    /**
     * Calculates longest squared euclidean distance between point from one set to point in the other set. This measure is also known as
     * Hausdorff Distance
     * @param points
     * @param otherPoints
     * @return
     */
    public static double calculateLongestDistance(final List<Point2D> points, final List<Point2D> otherPoints) {
        if (points == null || otherPoints == null)
            throw new IllegalArgumentException("One of passed points lists is not provided");

        double maxDistance = 0;
        for (Point2D point : points) {
            double minDistance = Double.MAX_VALUE;

            for (Point2D otherPoint : otherPoints) {
                double dX = point.getX() - otherPoint.getX();
                double dY = point.getY() - otherPoint.getY();
                double currentDistance = dX*dX + dY*dY;

                if (currentDistance < minDistance){
                    minDistance = currentDistance;
                }

                if ( currentDistance == 0 ){
                    minDistance = 0;
                    break;
                }
            }
            if (minDistance > maxDistance)
                maxDistance = minDistance;
        }
        return maxDistance;
    }
}
