package mappers;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

import java.awt.geom.Point2D;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Used to map CSV file points column
 */
public class PointsStringToListMapper extends AbstractBeanField {
    public static final String POINTS_ARRAY_PATTERN = "\\[(.+?)\\]";
    public static final int POINTS_GROUP_ID = 1;

    @Override
    protected List<Point2D> convert(String pointsAsString) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        if (pointsAsString == null || pointsAsString.length() == 0)
            return List.of();

        String pointWithoutParentParenthesis = pointsAsString.substring(1, pointsAsString.length() - 1);
        Pattern pointsArrayPattern = Pattern.compile(POINTS_ARRAY_PATTERN);
        Matcher pointsArrayMatcher = pointsArrayPattern.matcher(pointWithoutParentParenthesis);

        List<String[]> points = new LinkedList<>();
        while (pointsArrayMatcher.find()) {
            points.add(pointsArrayMatcher.group(POINTS_GROUP_ID).split(","));
        }

        return points.stream()
                .map(pointsArray -> new Point2D.Double(Double.parseDouble(pointsArray[0]), Double.parseDouble(pointsArray[1])))
                .collect(Collectors.toList());
    }
}
