import importers.RoutesImporter;
import logic.DistanceCalculator;
import logic.RepresentativeRouteFinder;
import models.Route;

import java.util.List;

public class Main {
    public static final String DEFAULT_FILE_NAME = "routes.csv";

    /**
     * Entry point
     * @param args
     */
    public static void main(String[] args) {
        String routesFileName = args.length > 0 ? args[0] : DEFAULT_FILE_NAME;
        DistanceCalculator distanceCalculator = new DistanceCalculator();

        List<Route> routesFromCsv = RoutesImporter.importRoutesFromCsv(routesFileName);
        String routeId = new RepresentativeRouteFinder(distanceCalculator)
                .getIdOfRouteWithTheShortestTotalDistance(routesFromCsv);

        System.out.println("Representative route's ID: " + routeId);
    }
}
