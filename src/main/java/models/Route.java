package models;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import mappers.PointsStringToListMapper;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Entity for route imported from CSV file.
 * Immutable.
 */
public class Route {
    @CsvBindByName()
    private String from_seq;
    @CsvBindByName()
    private String to_seq;
    @CsvCustomBindByName(converter = PointsStringToListMapper.class)
    private List<Point2D> points;

    /**
     * Used by CSV mapper
     */
    public Route() { }

    // Used for tests
    public Route(String id, List<Point2D> points) {
        this.from_seq = this.to_seq = id;
        this.points = points;
    }

    public String getId() {
        return from_seq + "_" + to_seq;
    }

    public List<Point2D> getPoints() {
        return points.stream()
                .map(point -> (Point2D) point.clone())
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return getId().equals(route.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
