package models;

/**
 * Represents distance between two routes.
 * Immutable.
 */
public class RouteDistance {
    private final String routeId;
    private final String targetRouteId;
    private final double distance;

    public RouteDistance(String routeId, String targetRouteId, double distance) {
        this.routeId = routeId;
        this.targetRouteId = targetRouteId;
        this.distance = distance;
    }

    public String getRouteId() {
        return routeId;
    }

    public String getTargetRouteId() {
        return targetRouteId;
    }

    public double getDistance() {
        return distance;
    }
}
